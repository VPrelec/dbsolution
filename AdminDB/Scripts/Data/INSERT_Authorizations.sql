INSERT [AdminDB].[dbo].[Authorizations] ([AuthorizationID], [AuthorizationTitle], [UserCreated], [DateCreated], [UserModified], [DateModified]) VALUES (1, N'CAN_READ', N'Admin', CAST(N'2019-03-18T10:42:21.783' AS DateTime), NULL, NULL)
, (2, N'CAN_WRITE', N'Admin', CAST(N'2019-03-18T10:42:21.783' AS DateTime), NULL, NULL)
, (3, N'CAN_SAVE', N'Admin', CAST(N'2019-03-18T10:42:21.783' AS DateTime), NULL, NULL)
, (4, N'CAN_OPEN', N'Admin', CAST(N'2019-03-18T10:42:21.783' AS DateTime), NULL, NULL)
, (5, N'CAN_DELETE', N'Admin', CAST(N'2019-03-18T10:42:21.783' AS DateTime), NULL, NULL)

