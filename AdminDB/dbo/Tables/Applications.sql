﻿CREATE TABLE [dbo].[Applications] (
    [ApplicationID]    INT          IDENTITY (1, 1) NOT NULL,
    [ApplicationTitle] VARCHAR (20) NOT NULL,
    [UserCreated]      VARCHAR (20) NOT NULL,
    [DateCreated]      DATETIME     NOT NULL,
    [UserModified]     VARCHAR (20) NULL,
    [DateModified]     DATETIME     NULL,
    CONSTRAINT [PK_Applications] PRIMARY KEY CLUSTERED ([ApplicationID] ASC)
);


GO
-- =============================================
-- Author:		Vinko Prelec
-- Create date: 27.03.2019
-- Description:	Inserts into Applications_History
-- =============================================
CREATE TRIGGER dbo.Applications_History_InsertDelete 
   ON  dbo.Applications 
   FOR UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- Ako je UPDATE
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		INSERT INTO AdminDB.dbo.Applications_History (ApplicationTitle, ApplicationID, UserCreated, DateCreated, UserModified, DateModified, ChangeType)
		SELECT Applications.ApplicationTitle, Applications.ApplicationID, Applications.UserCreated, Applications.DateCreated, Applications.UserModified, Applications.DateModified, 'U'
		FROM dbo.Applications INNER JOIN inserted I ON Applications.ApplicationID = I.ApplicationID
	END
	ELSE
	-- Ako je DELETE
	IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		INSERT INTO AdminDB.dbo.Applications_History (ApplicationTitle, ApplicationID, UserCreated, DateCreated, UserModified, DateModified, ChangeType)
		SELECT Applications.ApplicationTitle, Applications.ApplicationID, Applications.UserCreated, Applications.DateCreated, Applications.UserModified, Applications.DateModified, 'D'
		FROM dbo.Applications INNER JOIN inserted I ON Applications.ApplicationID = I.ApplicationID
	END
END