﻿CREATE TABLE [dbo].[Roles] (
    [RoleID]       INT          IDENTITY (1, 1) NOT NULL,
    [RoleTitle]    VARCHAR (20) NOT NULL,
    [UserCreated]  VARCHAR (20) NOT NULL,
    [DateCreated]  DATETIME     NOT NULL,
    [UserModified] VARCHAR (20) NULL,
    [DateModified] DATETIME     NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([RoleID] ASC)
);


GO
-- =============================================
-- Author:		Vinko Prelec
-- Create date: 27.03.2019
-- Description:	Inserts into Applications_History
-- =============================================
CREATE TRIGGER dbo.Roles_History_InsertDelete 
   ON  dbo.Roles 
   FOR UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- Ako je UPDATE
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		INSERT INTO AdminDB.dbo.Roles_History(RoleTitle, RoleID, UserCreated, DateCreated, UserModified, DateModified, ChangeType)
		SELECT Roles.RoleTitle, Roles.RoleID, Roles.UserCreated, Roles.DateCreated, Roles.UserModified, Roles.DateModified, 'U'
		FROM dbo.Roles INNER JOIN inserted I ON Roles.RoleID = I.RoleID
	END
	ELSE
	-- Ako je DELETE
	IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		INSERT INTO AdminDB.dbo.Roles_History(RoleTitle, RoleID, UserCreated, DateCreated, UserModified, DateModified, ChangeType)
		SELECT Roles.RoleTitle, Roles.RoleID, Roles.UserCreated, Roles.DateCreated, Roles.UserModified, Roles.DateModified, 'D'
		FROM dbo.Roles INNER JOIN inserted I ON Roles.RoleID = I.RoleID
	END
END