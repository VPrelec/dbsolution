﻿CREATE TABLE [dbo].[Users] (
    [UserID]       INT           IDENTITY (1, 1) NOT NULL,
    [Username]     VARCHAR (100) NOT NULL,
    [PasswordHash] VARCHAR (200) NOT NULL,
    [IsActive]     BIT           NOT NULL,
    [IsRegistered] BIT           NOT NULL,
    [UserCreated]  VARCHAR (20)  NOT NULL,
    [DateCreated]  DATETIME      NOT NULL,
    [UserModified] VARCHAR (20)  NULL,
    [DateModified] DATETIME      NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserID] ASC)
);


GO
-- =============================================
-- Author:		Vinko Prelec
-- Create date: 27.03.2019
-- Description:	Inserts into Users_History
-- =============================================
CREATE TRIGGER dbo.Users_History_InsertDelete 
   ON  dbo.Users 
   FOR UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- Ako je UPDATE
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		INSERT INTO AdminDB.dbo.Users_History(Username, UserID, UserCreated, DateCreated, UserModified, DateModified, ChangeType, PasswordHash, IsActive, IsRegistered)
		SELECT Users.Username, Users.UserID, Users.UserCreated, Users.DateCreated, Users.UserModified, Users.DateModified, 'U', Users.PasswordHash, Users.IsActive, Users.IsRegistered
		FROM dbo.Users INNER JOIN inserted I ON Users.UserID = I.UserID
	END
	ELSE
	-- Ako je DELETE
	IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		INSERT INTO AdminDB.dbo.Users_History(Username, UserID, UserCreated, DateCreated, UserModified, DateModified, ChangeType, PasswordHash, IsActive, IsRegistered)
		SELECT Users.Username, Users.UserID, Users.UserCreated, Users.DateCreated, Users.UserModified, Users.DateModified, 'D', Users.PasswordHash, Users.IsActive, Users.IsRegistered
		FROM dbo.Users INNER JOIN inserted I ON Users.UserID = I.UserID
	END
END