﻿CREATE TABLE [dbo].[Authorizations] (
    [AuthorizationID]    INT          IDENTITY (1, 1) NOT NULL,
    [AuthorizationTitle] VARCHAR (20) NOT NULL,
    [UserCreated]        VARCHAR (20) NOT NULL,
    [DateCreated]        DATETIME     NOT NULL,
    [UserModified]       VARCHAR (20) NULL,
    [DateModified]       DATETIME     NULL,
    CONSTRAINT [PK_Authorizations] PRIMARY KEY CLUSTERED ([AuthorizationID] ASC)
);


GO
-- =============================================
-- Author:		Vinko Prelec
-- Create date: 27.03.2019
-- Description:	Inserts into Applications_History
-- =============================================
CREATE TRIGGER dbo.Authorizations_History_InsertDelete 
   ON  dbo.Authorizations 
   FOR UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- Ako je UPDATE
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		INSERT INTO AdminDB.dbo.Authorizations_History(AuthorizationTitle, AuthorizationID, UserCreated, DateCreated, UserModified, DateModified, ChangeType)
		SELECT Authorizations.AuthorizationTitle, Authorizations.AuthorizationID, Authorizations.UserCreated, Authorizations.DateCreated, Authorizations.UserModified, Authorizations.DateModified, 'U'
		FROM dbo.Authorizations INNER JOIN inserted I ON Authorizations.AuthorizationID = I.AuthorizationID
	END
	ELSE
	-- Ako je DELETE
	IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		INSERT INTO AdminDB.dbo.Authorizations_History(AuthorizationTitle, AuthorizationID, UserCreated, DateCreated, UserModified, DateModified, ChangeType)
		SELECT Authorizations.AuthorizationTitle, Authorizations.AuthorizationID, Authorizations.UserCreated, Authorizations.DateCreated, Authorizations.UserModified, Authorizations.DateModified, 'D'
		FROM dbo.Authorizations INNER JOIN inserted I ON Authorizations.AuthorizationID = I.AuthorizationID
	END
END