﻿CREATE TABLE [dbo].[RoleAuthorizations] (
    [RoleAuthorizationID] INT IDENTITY (1, 1) NOT NULL,
    [RoleID]              INT NOT NULL,
    [AuthorizationID]     INT NOT NULL,
    [UserID]              INT NOT NULL,
    CONSTRAINT [PK_RoleAuthorizations] PRIMARY KEY CLUSTERED ([RoleAuthorizationID] ASC),
    CONSTRAINT [FK_RoleAuthorizations_Authorizations] FOREIGN KEY ([AuthorizationID]) REFERENCES [dbo].[Authorizations] ([AuthorizationID]),
    CONSTRAINT [FK_RoleAuthorizations_Roles] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Roles] ([RoleID]),
    CONSTRAINT [FK_RoleAuthorizations_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);



