﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 29.3.2019
-- Description:	Function which returns table of authorizations for certain user
-- =============================================
CREATE FUNCTION [dbo].[fn_GetAuthorizationsForUser] 
(
	-- Add the parameters for the function here
	@UserID INT, 
	@AuthorizationName VARCHAR(20)
)
RETURNS 
@ResTable TABLE 
(
	-- Add the column definitions for the TABLE variable here
	UserID INT, 
	AuthName VARCHAR(20)
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	INSERT INTO @ResTable
		(UserID, AuthName)
	SELECT AuthorizationTitle, UserID
	FROM AdminDB.dbo.RoleAuthorizations INNER JOIN AdminDB.dbo.Authorizations ON RoleAuthorizations.AuthorizationID = Authorizations.AuthorizationID
	WHERE UserID = @UserID
	RETURN 
END