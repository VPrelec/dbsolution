﻿
CREATE FUNCTION fn_CanUserAccessApp
(
	-- Add the parameters for the function here
	@UserID INT,
	@ApplicationName VARCHAR(20)
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Res BIT

	-- Add the T-SQL statements to compute the return value here
	SELECT @Res = @UserID
	FROM AdminDB.dbo.ApplicationUsers a INNER JOIN AdminDB.dbo.Applications b ON a.ApplicationID = b.ApplicationID
	WHERE (a.UserID = @UserID) 
		AND (b.ApplicationTitle = @ApplicationName)

	-- Return the result of the function
	IF (@Res IS NULL)
		SET @Res = 0
	IF (@Res IS NOT NULL)
		SET @Res = 1

	RETURN @Res

END