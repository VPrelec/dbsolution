﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 27.03.2019
-- Description:	Function which returns what day type it is
-- =============================================
CREATE FUNCTION [dbo].[fn_GetDayType] 
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS CHAR
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result CHAR(1)

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = Calendar.TypeOfDay
	FROM AdminDB.dbo.Calendar
	WHERE Calendar.Date = @Date
	
	-- Return the result of the function
	IF (@Result IS NULL)
		SET @Result = 0
	RETURN @Result

END