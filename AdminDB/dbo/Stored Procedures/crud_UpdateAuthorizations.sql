﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for updating data in dbo.Authorizations
-- =============================================
CREATE PROCEDURE [dbo].[crud_UpdateAuthorizations]
	-- Add the parameters for the stored procedure here

	@AuthorizationID INT,
	@AuthorizationTitle VARCHAR(20),
	@UserModified VARCHAR(20),
	@DateModified DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Authorizations] 
	SET [AuthorizationTitle] = @AuthorizationTitle
		, [UserModified] = @UserModified
		, [DateModified] = @DateModified
	WHERE [AuthorizationID] = @AuthorizationID
END