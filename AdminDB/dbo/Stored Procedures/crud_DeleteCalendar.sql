﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for deleting data in dbo.Calendar
-- =============================================
CREATE PROCEDURE crud_DeleteCalendar
	-- Add the parameters for the stored procedure here

	@Date DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[Calendar] 
	WHERE [Date] = @Date
END