﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for inserting data into dbo.Users
-- =============================================
CREATE PROCEDURE [dbo].[crud_InsertUsers]
	-- Add the parameters for the stored procedure here
	@UserName VARCHAR(100),
	@PasswordHash VARCHAR(200),
	@IsActive BIT,
	@IsRegistered BIT,
	@UserCreated VARCHAR(20),
	@DateCreated DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Users] ([Username], [PasswordHash], [IsActive], [IsRegistered], [UserCreated], [DateCreated])
	VALUES (@UserName, @PasswordHash, @IsActive, @IsRegistered, @UserCreated, @DateCreated)
END