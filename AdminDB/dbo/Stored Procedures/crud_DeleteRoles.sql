﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for deleting data in dbo.Roles
-- =============================================
CREATE PROCEDURE [dbo].[crud_DeleteRoles]
	-- Add the parameters for the stored procedure here

	@RoleID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[Roles] 
	WHERE [RoleID] = @RoleID
END