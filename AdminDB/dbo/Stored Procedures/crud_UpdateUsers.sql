﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for updating data in dbo.Users
-- =============================================
CREATE PROCEDURE [dbo].[crud_UpdateUsers]
	-- Add the parameters for the stored procedure here

	@UserID INT,
	@Username VARCHAR(100),
	@PasswordHash VARCHAR(200),
	@IsActive BIT,
	@IsRegistered BIT,
	@UserModified VARCHAR(20),
	@DateModified DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Users] 
	SET [Username] = @Username
		, [PasswordHash] = @PasswordHash
		, [IsActive] = @IsActive
		, [IsRegistered] = @IsRegistered
		, [UserModified] = @UserModified
		, [DateModified] = @DateModified
	WHERE [UserID] = @UserID
END