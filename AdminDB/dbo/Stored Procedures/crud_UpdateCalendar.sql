﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for updating data in dbo.Calendar
-- =============================================
CREATE PROCEDURE [dbo].[crud_UpdateCalendar]
	-- Add the parameters for the stored procedure here

	@Date DATETIME,
	@Day VARCHAR(10),
	@TypeOfDay CHAR(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Calendar] 
	SET [Day] = @Day
		, [TypeOfDay] = @TypeOfDay
	WHERE [Date] = @Date
END