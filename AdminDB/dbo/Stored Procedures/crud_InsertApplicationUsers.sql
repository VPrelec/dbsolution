﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for inserting data into dbo.ApplicationUsers
-- =============================================
CREATE PROCEDURE [dbo].[crud_InsertApplicationUsers]
	-- Add the parameters for the stored procedure here
	@UserID INT, 
	@ApplicationID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[ApplicationUsers] ([UserID], [ApplicationID])
	VALUES (@UserID, @ApplicationID)
END