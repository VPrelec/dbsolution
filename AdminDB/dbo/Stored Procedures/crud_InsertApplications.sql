﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for inserting data into dbo.Applications
-- =============================================
CREATE PROCEDURE crud_InsertApplications 
	-- Add the parameters for the stored procedure here
	@ApplicationTitle VARCHAR(20), 
	@UserCreated VARCHAR(20),
	@DateCreated DATETIME 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Applications] ([ApplicationTitle], [UserCreated], [DateCreated])
	VALUES (@ApplicationTitle, @UserCreated, @DateCreated)
END