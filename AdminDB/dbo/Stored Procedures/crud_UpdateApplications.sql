﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for updating data in dbo.Applications
-- =============================================
CREATE PROCEDURE [dbo].[crud_UpdateApplications]
	-- Add the parameters for the stored procedure here

	@ApplicationID INT,
	@ApplicationTitle VARCHAR(20),
	@UserModified VARCHAR(20),
	@DateModified DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Applications] 
	SET [ApplicationTitle] = @ApplicationTitle
		, [UserModified] = @UserModified
		, [DateModified] = @DateModified
	WHERE [ApplicationID] = @ApplicationID
END