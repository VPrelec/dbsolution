﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for deleting data in dbo.ApplicationUsers
-- =============================================
CREATE PROCEDURE crud_DeleteApplicationUsers
	-- Add the parameters for the stored procedure here

	@ApplicationUserID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[ApplicationUsers] 
	WHERE [ApplicationUserID] = @ApplicationUserID
END