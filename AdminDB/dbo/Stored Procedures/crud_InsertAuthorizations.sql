﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for inserting data into dbo.Authorizations
-- =============================================
CREATE PROCEDURE [dbo].[crud_InsertAuthorizations]
	-- Add the parameters for the stored procedure here
	@AuthorizationTitle VARCHAR(20), 
	@UserCreated VARCHAR(20),
	@DateCreated DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Authorizations] ([AuthorizationTitle], [UserCreated], [DateCreated])
	VALUES (@AuthorizationTitle, @UserCreated, @DateCreated)
END