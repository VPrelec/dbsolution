﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for updating data in dbo.Roles
-- =============================================
CREATE PROCEDURE [dbo].[crud_UpdateRoles]
	-- Add the parameters for the stored procedure here

	@RoleID INT,
	@RoleTitle VARCHAR(20),
	@UserModified VARCHAR(20),
	@DateModified DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Roles] 
	SET [RoleTitle] = @RoleTitle
		, [UserModified] = @UserModified
		, [DateModified] = @DateModified
	WHERE [RoleID] = @RoleID
END