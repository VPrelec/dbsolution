﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019.
-- Description:	Procedure for updating data in dbo.ApplicationUsers
-- =============================================
CREATE PROCEDURE crud_UpdateApplicationUsers
	-- Add the parameters for the stored procedure here

	@ApplicationUserID INT,
	@UserID VARCHAR(20),
	@ApplicationID VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[ApplicationUsers] 
	SET [UserID] = @UserID
		, [ApplicationID] = @ApplicationID
	WHERE [ApplicationUserID] = @ApplicationUserID
END