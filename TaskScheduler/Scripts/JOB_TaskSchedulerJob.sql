USE LoggingDB

IF OBJECT_ID('tempdb..#TMP_Log') IS NOT NULL
BEGIN
	DROP TABLE #TMP_Log
END

SELECT * 
INTO #TMP_Log
FROM [LoggingDB].[dbo].[Log]

INSERT INTO [LoggingDB].[dbo].[Log_History]
	(
		[Log_History].[LogID], [Log_History].[ApplicationName], [Log_History].[Message], [Log_History].[MessageType], [Log_History].[UserCreated], [Log_History].[LogDateTimeStamp], [Log_History].[DateMoved]
	)
SELECT #TMP_Log.LogID, #TMP_Log.ApplicationName, #TMP_Log.[Message], #TMP_Log.MessageType, #TMP_Log.[UserCreated], #TMP_Log.LogDateTimeStamp, GETDATE()
FROM #TMP_Log
WHERE [LogDateTimeStamp] < DATEADD(Hour, 1, (DATEADD(DAY, -730, GETDATE())))

DROP TABLE #TMP_Log