﻿CREATE TABLE [dbo].[NationalIDTypes] (
    [NationalIDTypeID]    INT           IDENTITY (1, 1) NOT NULL,
    [NationalIDTypeTitle] VARCHAR (100) NOT NULL,
    [UserCreated]         VARCHAR (100) NOT NULL,
    [DateCreated]         DATETIME      NOT NULL,
    [UserModified]        VARCHAR (20)  NULL,
    [DateModified]        DATETIME      NULL,
    CONSTRAINT [PK_NationalIDTypes] PRIMARY KEY CLUSTERED ([NationalIDTypeID] ASC)
);

