﻿CREATE TABLE [dbo].[Districts] (
    [DistrictID]    INT           IDENTITY (1, 1) NOT NULL,
    [RegionID]      INT           NOT NULL,
    [DistrictType]  VARCHAR (20)  NOT NULL,
    [DistrictTitle] VARCHAR (255) NULL,
    [UserCreated]   VARCHAR (100) NOT NULL,
    [DateCreated]   DATETIME      NOT NULL,
    [UserModified]  VARCHAR (20)  NULL,
    [DateModified]  DATETIME      NULL,
    CONSTRAINT [PK_Districts] PRIMARY KEY CLUSTERED ([DistrictID] ASC),
    CONSTRAINT [FK_Districts_Regions] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[Regions] ([RegionID])
);


GO
-- =============================================
-- Author:		Vinko Prelec
-- Create date: 27.03.2019
-- Description:	Inserts into Districts_History
-- =============================================
CREATE TRIGGER dbo.Districts_History_InsertDelete 
   ON  dbo.Districts 
   FOR UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- Ako je UPDATE
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		INSERT INTO CoreDB.dbo.Districts_History(DistrictID, RegionID, UserCreated, DateCreated, UserModified, DateModified, ChangeType, DistrictType, DistrictTitle)
		SELECT Districts.DistrictID, Districts.RegionID, Districts.UserCreated, Districts.DateCreated, Districts.UserModified, Districts.DateModified, 'U', Districts.DistrictType, Districts.DistrictTitle
		FROM dbo.Districts INNER JOIN inserted I ON Districts.DistrictID = I.DistrictID
	END
	ELSE
	-- Ako je DELETE
	IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		INSERT INTO CoreDB.dbo.Districts_History(DistrictID, RegionID, UserCreated, DateCreated, UserModified, DateModified, ChangeType, DistrictType, DistrictTitle)
		SELECT Districts.DistrictID, Districts.RegionID, Districts.UserCreated, Districts.DateCreated, Districts.UserModified, Districts.DateModified, 'D', Districts.DistrictType, Districts.DistrictTitle
		FROM dbo.Districts INNER JOIN inserted I ON Districts.DistrictID = I.DistrictID
	END
END