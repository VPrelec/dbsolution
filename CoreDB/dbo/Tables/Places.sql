﻿CREATE TABLE [dbo].[Places] (
    [PlaceID]           INT           IDENTITY (1, 1) NOT NULL,
    [RegionID]          INT           NOT NULL,
    [DistrictID]        INT           NOT NULL,
    [PlaceNationalCode] VARCHAR (20)  NULL,
    [PlaceTitle]        VARCHAR (255) NOT NULL,
    [UserCreated]       VARCHAR (100) NOT NULL,
    [DateCreated]       DATETIME      NOT NULL,
    [UserModified]      VARCHAR (20)  NULL,
    [DateModified]      DATETIME      NULL,
    CONSTRAINT [PK_Places] PRIMARY KEY CLUSTERED ([PlaceID] ASC),
    CONSTRAINT [FK_Places_Districts] FOREIGN KEY ([DistrictID]) REFERENCES [dbo].[Districts] ([DistrictID]),
    CONSTRAINT [FK_Places_Regions] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[Regions] ([RegionID])
);


GO
-- =============================================
-- Author:		Vinko Prelec
-- Create date: 27.03.2019
-- Description:	Inserts into Places_History
-- =============================================
CREATE TRIGGER dbo.Places_History_InsertDelete 
   ON  dbo.Places 
   FOR UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- Ako je UPDATE
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		INSERT INTO CoreDB.dbo.Places_History(PlaceID, RegionID, UserCreated, DateCreated, UserModified, DateModified, ChangeType, DistrictID, PlaceNationalCode, PlaceTitle)
		SELECT Places.PlaceID, Places.RegionID, Places.UserCreated, Places.DateCreated, Places.UserModified, Places.DateModified, 'U', Places.DistrictID, Places.PlaceNationalCode, Places.PlaceTitle
		FROM dbo.Places INNER JOIN inserted I ON Places.PlaceID = I.PlaceID
	END
	ELSE
	-- Ako je DELETE
	IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		INSERT INTO CoreDB.dbo.Places_History(PlaceID, RegionID, UserCreated, DateCreated, UserModified, DateModified, ChangeType, DistrictID, PlaceNationalCode, PlaceTitle)
		SELECT Places.PlaceID, Places.RegionID, Places.UserCreated, Places.DateCreated, Places.UserModified, Places.DateModified, 'D', Places.DistrictID, Places.PlaceNationalCode, Places.PlaceTitle
		FROM dbo.Places INNER JOIN inserted I ON Places.PlaceID = I.PlaceID
	END
END