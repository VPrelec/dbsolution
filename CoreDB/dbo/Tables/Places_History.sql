﻿CREATE TABLE [dbo].[Places_History] (
    [RegionID]           INT           NOT NULL,
    [DistrictID]         INT           NOT NULL,
    [PlaceNationalCode ] VARCHAR (20)  NULL,
    [PlaceTitle]         VARCHAR (255) NOT NULL,
    [UserCreated]        VARCHAR (100) NOT NULL,
    [DateCreated]        DATETIME      NOT NULL,
    [UserModified]       VARCHAR (20)  NULL,
    [DateModified]       DATETIME      NULL,
    [ChangeType]         CHAR (1)      NOT NULL,
    [PlaceID]            INT           DEFAULT ((0)) NOT NULL
);

