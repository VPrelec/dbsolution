﻿CREATE TABLE [dbo].[Districts_History] (
    [RegionID]      INT           NOT NULL,
    [DistrictType]  VARCHAR (20)  NOT NULL,
    [DistrictTitle] VARCHAR (255) NULL,
    [UserCreated]   VARCHAR (100) NOT NULL,
    [DateCreated]   DATETIME      NOT NULL,
    [UserModified]  VARCHAR (20)  NULL,
    [DateModified]  DATETIME      NULL,
    [ChangeType]    CHAR (1)      NOT NULL,
    [DistrictID]    INT           DEFAULT ((0)) NOT NULL
);

