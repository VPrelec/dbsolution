﻿CREATE TABLE [dbo].[Employees] (
    [EmployeeID]       INT           IDENTITY (1, 1) NOT NULL,
    [Username]         VARCHAR (20)  NOT NULL,
    [Firstname]        VARCHAR (255) NOT NULL,
    [Lastname]         VARCHAR (255) NOT NULL,
    [NationalIDNumber] VARCHAR (100) NOT NULL,
    [NationalIDType]   INT           NOT NULL,
    [GenderID]         INT           NOT NULL,
    [Birthdate]        DATE          NULL,
    [Address]          VARCHAR (100) NULL,
    [PlaceID]          INT           NOT NULL,
    [CountryID]        INT           NOT NULL,
    [UserCreated]      VARCHAR (100) NOT NULL,
    [DateCreated]      DATETIME      NOT NULL,
    [UserModified]     VARCHAR (100) NULL,
    [DateModified]     DATETIME      NULL,
    CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED ([EmployeeID] ASC),
    CONSTRAINT [FK_Employees_Countries] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Countries] ([CountryID]),
    CONSTRAINT [FK_Employees_Genders] FOREIGN KEY ([GenderID]) REFERENCES [dbo].[Genders] ([GenderID]),
    CONSTRAINT [FK_Employees_NationalIDTypes] FOREIGN KEY ([NationalIDType]) REFERENCES [dbo].[NationalIDTypes] ([NationalIDTypeID]),
    CONSTRAINT [FK_Employees_Places] FOREIGN KEY ([PlaceID]) REFERENCES [dbo].[Places] ([PlaceID])
);


GO
-- =============================================
-- Author:		Vinko Prelec
-- Create date: 27.03.2019
-- Description:	Inserts into Employees_History
-- =============================================
CREATE TRIGGER dbo.Employees_History_InsertDelete 
   ON  dbo.Employees 
   FOR UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- Ako je UPDATE
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		INSERT INTO CoreDB.dbo.Employees_History(EmployeeID, Username, UserCreated, DateCreated, UserModified, DateModified, ChangeType, Firstname, Lastname, NationalIDNumber, NationalIDType, GenderID, Birthdate, [Address], PlaceID, CountryID)
		SELECT Employees.EmployeeID, Employees.Username, Employees.UserCreated, Employees.DateCreated, Employees.UserModified, Employees.DateModified, 'U', Employees.Firstname, Employees.Lastname, Employees.NationalIDNumber, Employees.NationalIDType, Employees.GenderID, Employees.Birthdate, Employees.[Address], Employees.PlaceID, Employees.CountryID
		FROM dbo.Employees INNER JOIN inserted I ON Employees.EmployeeID = I.EmployeeID
	END
	ELSE
	-- Ako je DELETE
	IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		INSERT INTO CoreDB.dbo.Employees_History(EmployeeID, Username, UserCreated, DateCreated, UserModified, DateModified, ChangeType, Firstname, Lastname, NationalIDNumber, NationalIDType, GenderID, Birthdate, [Address], PlaceID, CountryID)
		SELECT Employees.EmployeeID, Employees.Username, Employees.UserCreated, Employees.DateCreated, Employees.UserModified, Employees.DateModified, 'D', Employees.Firstname, Employees.Lastname, Employees.NationalIDNumber, Employees.NationalIDType, Employees.GenderID, Employees.Birthdate, Employees.[Address], Employees.PlaceID, Employees.CountryID
		FROM dbo.Employees INNER JOIN inserted I ON Employees.EmployeeID = I.EmployeeID
	END
END