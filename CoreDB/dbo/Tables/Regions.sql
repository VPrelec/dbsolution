﻿CREATE TABLE [dbo].[Regions] (
    [RegionID]     INT           IDENTITY (1, 1) NOT NULL,
    [CountryID]    INT           NOT NULL,
    [RegionTitle]  VARCHAR (255) NOT NULL,
    [UserCreated]  VARCHAR (100) NOT NULL,
    [DateCreated]  DATETIME      NOT NULL,
    [UserModified] VARCHAR (20)  NULL,
    [DateModified] DATETIME      NULL,
    CONSTRAINT [PK_Regions] PRIMARY KEY CLUSTERED ([RegionID] ASC),
    CONSTRAINT [FK_Regions_Countries] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Countries] ([CountryID])
);


GO
-- =============================================
-- Author:		Vinko Prelec
-- Create date: 27.03.2019
-- Description:	Inserts into Regions_History
-- =============================================
CREATE TRIGGER dbo.Regions_History_InsertDelete 
   ON  dbo.Regions 
   FOR UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- Ako je UPDATE
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		INSERT INTO CoreDB.dbo.Regions_History(RegionID, CountryID, UserCreated, DateCreated, UserModified, DateModified, ChangeType, RegionTitle)
		SELECT Regions.RegionID, Regions.CountryID, Regions.UserCreated, Regions.DateCreated, Regions.UserModified, Regions.DateModified, 'U', Regions.RegionTitle
		FROM dbo.Regions INNER JOIN inserted I ON Regions.RegionID = I.RegionID
	END
	ELSE
	-- Ako je DELETE
	IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		INSERT INTO CoreDB.dbo.Regions_History(RegionID, CountryID, UserCreated, DateCreated, UserModified, DateModified, ChangeType, RegionTitle)
		SELECT Regions.RegionID, Regions.CountryID, Regions.UserCreated, Regions.DateCreated, Regions.UserModified, Regions.DateModified, 'D', Regions.RegionTitle
		FROM dbo.Regions INNER JOIN inserted I ON Regions.RegionID = I.RegionID
	END
END