﻿CREATE TABLE [dbo].[Regions_History] (
    [CountryID]    INT           NOT NULL,
    [RegionTitle]  VARCHAR (255) NOT NULL,
    [UserCreated]  VARCHAR (100) NOT NULL,
    [DateCreated]  DATETIME      NOT NULL,
    [UserModified] VARCHAR (20)  NULL,
    [DateModified] DATETIME      NULL,
    [ChangeType]   CHAR (1)      NOT NULL,
    [RegionID]     INT           DEFAULT ((0)) NOT NULL
);

