﻿CREATE TABLE [dbo].[Employees_History] (
    [Username]         VARCHAR (20)  NOT NULL,
    [Firstname]        VARCHAR (255) NULL,
    [Lastname]         VARCHAR (255) NULL,
    [NationalIDNumber] VARCHAR (100) NOT NULL,
    [NationalIDType]   INT           NOT NULL,
    [GenderID]         INT           NOT NULL,
    [Birthdate]        DATE          NULL,
    [Address]          VARCHAR (100) NULL,
    [PlaceID]          INT           NULL,
    [CountryID]        INT           NULL,
    [UserCreated]      VARCHAR (100) NOT NULL,
    [DateCreated]      DATETIME      NOT NULL,
    [UserModified]     VARCHAR (100) NULL,
    [DateModified]     DATETIME      NULL,
    [ChangeType]       CHAR (1)      NOT NULL,
    [EmployeeID]       INT           DEFAULT ((0)) NOT NULL
);

