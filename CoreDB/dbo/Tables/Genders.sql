﻿CREATE TABLE [dbo].[Genders] (
    [GenderID]     INT           IDENTITY (1, 1) NOT NULL,
    [GenderTitle]  VARCHAR (255) NOT NULL,
    [GenderShort]  CHAR (5)      NOT NULL,
    [UserCreated]  VARCHAR (100) NOT NULL,
    [DateCreated]  DATETIME      NOT NULL,
    [UserModified] VARCHAR (20)  NULL,
    [DateModified] DATETIME      NULL,
    CONSTRAINT [PK_Genders] PRIMARY KEY CLUSTERED ([GenderID] ASC)
);


GO
-- =============================================
-- Author:		Vinko Prelec
-- Create date: 27.03.2019
-- Description:	Inserts into Genders_History
-- =============================================
CREATE TRIGGER dbo.Genders_History_InsertDelete 
   ON  dbo.Genders 
   FOR UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- Ako je UPDATE
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		INSERT INTO CoreDB.dbo.Genders_History(GenderID, GenderTitle, UserCreated, DateCreated, UserModified, DateModified, ChangeType, GenderShort)
		SELECT Genders.GenderID, Genders.GenderTitle, Genders.UserCreated, Genders.DateCreated, Genders.UserModified, Genders.DateModified, 'U', Genders.GenderShort
		FROM dbo.Genders INNER JOIN inserted I ON Genders.GenderID = I.GenderID
	END
	ELSE
	-- Ako je DELETE
	IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		INSERT INTO CoreDB.dbo.Genders_History(GenderID, GenderTitle, UserCreated, DateCreated, UserModified, DateModified, ChangeType, GenderShort)
		SELECT Genders.GenderID, Genders.GenderTitle, Genders.UserCreated, Genders.DateCreated, Genders.UserModified, Genders.DateModified, 'D', Genders.GenderShort
		FROM dbo.Genders INNER JOIN inserted I ON Genders.GenderID = I.GenderID
	END
END