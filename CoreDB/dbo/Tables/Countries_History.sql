﻿CREATE TABLE [dbo].[Countries_History] (
    [CountryCode]  CHAR (3)      NULL,
    [CountryTitle] VARCHAR (100) NOT NULL,
    [UserCreated]  VARCHAR (100) NOT NULL,
    [DateCreated]  DATETIME      NOT NULL,
    [UserModified] VARCHAR (20)  NULL,
    [DateModified] DATETIME      NULL,
    [ChangeType]   CHAR (1)      NOT NULL,
    [CountryID]    INT           DEFAULT ((0)) NOT NULL
);

