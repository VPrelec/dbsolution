﻿CREATE TABLE [dbo].[Genders_History] (
    [GenderTitle]  VARCHAR (255) NOT NULL,
    [GenderShort]  CHAR (5)      NOT NULL,
    [UserCreated]  VARCHAR (100) NOT NULL,
    [DateCreated]  DATETIME      NOT NULL,
    [UserModified] VARCHAR (20)  NULL,
    [DateModified] DATETIME      NULL,
    [ChangeType]   CHAR (1)      NOT NULL,
    [GenderID]     INT           DEFAULT ((0)) NOT NULL
);

