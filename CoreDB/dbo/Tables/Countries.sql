﻿CREATE TABLE [dbo].[Countries] (
    [CountryID]    INT           IDENTITY (1, 1) NOT NULL,
    [CountryCode]  CHAR (3)      NULL,
    [CountryTitle] VARCHAR (100) NOT NULL,
    [UserCreated]  VARCHAR (100) NOT NULL,
    [DateCreated]  DATETIME      NOT NULL,
    [UserModified] VARCHAR (20)  NULL,
    [DateModified] DATETIME      NULL,
    CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED ([CountryID] ASC)
);


GO
-- =============================================
-- Author:		Vinko Prelec
-- Create date: 27.03.2019
-- Description:	Inserts into Countries_History
-- =============================================
CREATE TRIGGER dbo.Countries_History_InsertDelete 
   ON  dbo.Countries 
   FOR UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- Ako je UPDATE
	IF EXISTS(SELECT * FROM deleted) AND EXISTS(SELECT * FROM inserted)
	BEGIN
		INSERT INTO CoreDB.dbo.Countries_History(CountryCode, CountryID, UserCreated, DateCreated, UserModified, DateModified, ChangeType, CountryTitle)
		SELECT Countries.CountryCode, Countries.CountryID, Countries.UserCreated, Countries.DateCreated, Countries.UserModified, Countries.DateModified, 'U', Countries.CountryTitle
		FROM dbo.Countries INNER JOIN inserted I ON Countries.CountryID = I.CountryID
	END
	ELSE
	-- Ako je DELETE
	IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		INSERT INTO CoreDB.dbo.Countries_History(CountryCode, CountryID, UserCreated, DateCreated, UserModified, DateModified, ChangeType, CountryTitle)
		SELECT Countries.CountryCode, Countries.CountryID, Countries.UserCreated, Countries.DateCreated, Countries.UserModified, Countries.DateModified, 'D', Countries.CountryTitle
		FROM dbo.Countries INNER JOIN inserted I ON Countries.CountryID = I.CountryID
	END
END