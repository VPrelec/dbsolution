﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019
-- Description:	Procedure for inserting data into dbo.Districts
-- =============================================
CREATE PROCEDURE crud_InsertDistricts 
	-- Add the parameters for the stored procedure here
	@RegionID INT, 
	@DistrictType VARCHAR(20), 
	@DistrictTitle VARCHAR(255), 
	@UserCreated VARCHAR(100), 
	@DateCreated DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Districts] ([RegionID], [DistrictType], [DistrictTitle], [UserCreated], [DateCreated])
	VALUES (@RegionID, @DistrictType, @DistrictTitle, @UserCreated, @DateCreated)
END