﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019
-- Description:	Procedure for inserting data into dbo.NationalIDTypes
-- =============================================
CREATE PROCEDURE crud_InsertNationalIDTypes
	-- Add the parameters for the stored procedure here
	@NationalIDTypeTitle VARCHAR(100),
	@UserCreated VARCHAR(20), 
	@DateCreated DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[NationalIDTypes] ([NationalIDTypeTitle], [UserCreated], [DateCreated])
	VALUES (@NationalIDTypeTitle, @UserCreated, @DateCreated)
END