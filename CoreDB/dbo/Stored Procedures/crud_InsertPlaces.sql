﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019
-- Description:	Procedure for inserting data into dbo.Places
-- =============================================
CREATE PROCEDURE crud_InsertPlaces
	-- Add the parameters for the stored procedure here
	@RegionID INT,
	@DistrictID INT,
	@PlaceNationalCode VARCHAR(20),
	@PlaceTitle VARCHAR(100),
	@UserCreated VARCHAR(20), 
	@DateCreated DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Places] ([RegionID], [DistrictID], [PlaceNationalCode], [PlaceTitle], [UserCreated], [DateCreated])
	VALUES (@RegionID, @DistrictID, @PlaceNationalCode, @PlaceTitle, @UserCreated, @DateCreated)
END