﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019
-- Description:	Procedure for deleting data from dbo.Places
-- =============================================
CREATE PROCEDURE crud_DeletePlaces
	-- Add the parameters for the stored procedure here
	@PlaceID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[Places]
	WHERE [PlaceID] = @PlaceID
END