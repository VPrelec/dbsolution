﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.3.2019.
-- Description:	Procedure for inserting data into dbo.Countries
-- =============================================
CREATE PROCEDURE crud_InsertCountries
	-- Add the parameters for the stored procedure here
	@CountryCode CHAR(3), 
	@CountryTitle VARCHAR(100),
	@UserCreated VARCHAR(100),
	@DateCreated DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Countries] ([CountryCode], [CountryTitle], [UserCreated], [DateCreated])
	VALUES (@CountryCode, @CountryTitle, @UserCreated, @DateCreated)	
END