﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.3.2019.
-- Description:	Procedure for updating data in dbo.Regions
-- =============================================
CREATE PROCEDURE crud_UpdateRegions
	-- Add the parameters for the stored procedure here
	@RegionID INT,
	@CountryID INT,
	@RegionTitle VARCHAR(255),
	@UserModified VARCHAR(20), 
	@DateModified DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Regions] 
	SET [CountryID] = @CountryID
		, [RegionTitle] = @RegionTitle
		, [UserModified] = @UserModified
		, [DateModified] = @DateModified 
	WHERE [RegionID] = @RegionID
END