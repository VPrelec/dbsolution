﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019
-- Description:	Procedure for inserting data into dbo.Genders
-- =============================================
CREATE PROCEDURE crud_InsertGenders
	-- Add the parameters for the stored procedure here
	@GenderTitle VARCHAR(255),
	@GenderShort CHAR(5),
	@UserCreated VARCHAR(20), 
	@DateCreated DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Genders] ([GenderTitle], [GenderShort], [UserCreated], [DateCreated])
	VALUES (@GenderTitle, @GenderShort, @UserCreated, @DateCreated)
END