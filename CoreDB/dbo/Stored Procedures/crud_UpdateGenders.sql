﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.3.2019.
-- Description:	Procedure for updating data in dbo.Genders
-- =============================================
CREATE PROCEDURE crud_UpdateGenders
	-- Add the parameters for the stored procedure here
	@GenderID INT,
	@GenderTitle VARCHAR(255),
	@GenderShort CHAR(5),
	@UserModified VARCHAR(20), 
	@DateModified DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Genders] 
	SET [GenderTitle] = @GenderTitle
		, [GenderShort] = @GenderShort
		, [UserModified] = @UserModified
		, [DateModified] = @DateModified 
	WHERE [GenderID] = @GenderID
END