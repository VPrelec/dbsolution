﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.3.2019.
-- Description:	Procedure for updating data in dbo.Places
-- =============================================
CREATE PROCEDURE [dbo].[crud_UpdatePlaces]
	-- Add the parameters for the stored procedure here
	@PlaceID INT,
	@RegionID INT,
	@DistrictID INT,
	@PlaceNationalCode VARCHAR(20),
	@PlaceTitle VARCHAR(100),
	@UserModified VARCHAR(20), 
	@DateModified DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Places] 
	SET [RegionID] = @RegionID
		, [DistrictID] = @DistrictID
		, [PlaceNationalCode] = @PlaceNationalCode
		, [PlaceTitle] = @PlaceTitle
		, [UserModified] = @UserModified
		, [DateModified] = @DateModified 
	WHERE [PlaceID] = @PlaceID
END