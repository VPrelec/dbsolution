﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019
-- Description:	Procedure for inserting data into dbo.Regions
-- =============================================
CREATE PROCEDURE crud_InsertRegions
	-- Add the parameters for the stored procedure here
	@CountryID INT,
	@RegionTitle VARCHAR(255),
	@UserCreated VARCHAR(20), 
	@DateCreated DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Regions] ([CountryID], [RegionTitle], [UserCreated], [DateCreated])
	VALUES (@CountryID, @RegionTitle, @UserCreated, @DateCreated)
END