﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019
-- Description:	Procedure for deleting data from dbo.Employees
-- =============================================
CREATE PROCEDURE crud_DeleteEmployees
	-- Add the parameters for the stored procedure here
	@EmployeeID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[Employees]
	WHERE [EmployeeID] = @EmployeeID
END