﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019
-- Description:	Procedure for deleting data from dbo.Regions
-- =============================================
CREATE PROCEDURE crud_DeleteRegions
	-- Add the parameters for the stored procedure here
	@RegionID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[Regions]
	WHERE [RegionID] = @RegionID
END