﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.3.2019.
-- Description:	Procedure for updating data in dbo.Districts
-- =============================================
CREATE PROCEDURE crud_UpdateDistricts
	-- Add the parameters for the stored procedure here
	@DistrictID INT,
	@RegionID INT,
	@DistrictType VARCHAR(20),
	@DistrictTitle VARCHAR(255),
	@UserModified VARCHAR(20),
	@DateModified DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Districts] 
	SET [RegionID] = @RegionID
		, [DistrictType] = @DistrictType
		, [DistrictTitle] = @DistrictTitle
		, [UserModified] = @UserModified 
		, [DateModified] = @DateModified
	WHERE [DistrictID] = @DistrictID
END