﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019
-- Description:	Procedure for deleting data from dbo.Districts
-- =============================================
CREATE PROCEDURE crud_DeleteDistricts
	-- Add the parameters for the stored procedure here
	@DistrictID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[Districts]
	WHERE [DistrictID] = @DistrictID
END