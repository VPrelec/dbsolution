﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.3.2019.
-- Description:	Procedure for updating data in dbo.Employees
-- =============================================
CREATE PROCEDURE crud_UpdateEmployees
	-- Add the parameters for the stored procedure here
	@EmployeeID INT,
	@UserName VARCHAR(20),
	@FirstName VARCHAR(255),
	@LastName VARCHAR(255),
	@NationalIdNumber VARCHAR(100), 
	@NationalIdType INT, 
	@GenderId INT,
	@BirthDate DATE,
	@Address VARCHAR(100),
	@PlaceId INT,
	@CountryId INT, 
	@UserModified VARCHAR(20), 
	@DateModified DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Employees] 
	SET [Username] = @UserName
		, [Firstname] = @FirstName
		, [Lastname] = @LastName
		, [NationalIDNumber] = @NationalIdNumber
		, [NationalIDType] = @NationalIdType
		, [GenderID] = @GenderId
		, [Birthdate] = @BirthDate
		, [Address] = @Address
		, [PlaceID] = @PlaceId
		, [CountryID] = @CountryId
		, [UserModified] = @UserModified
		, [DateModified] = @DateModified 
	WHERE [EmployeeID] = @EmployeeID
END