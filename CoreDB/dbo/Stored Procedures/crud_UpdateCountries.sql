﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.3.2019.
-- Description:	Procedure for updating data in dbo.Countries
-- =============================================
CREATE PROCEDURE [dbo].[crud_UpdateCountries]
	-- Add the parameters for the stored procedure here
	@CountryID INT,
	@CountryCode CHAR(3), 
	@CountryTitle VARCHAR(100),
	@UserModified VARCHAR(20),
	@DateModified DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Countries] 
	SET [CountryCode] = @CountryCode
		, [CountryTitle] = @CountryTitle 
		, [UserModified] = @UserModified 
		, [DateModified] = @DateModified
	WHERE [CountryID] = @CountryID
END