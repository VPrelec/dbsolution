﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.03.2019
-- Description:	Procedure for inserting data into dbo.Employees
-- =============================================
CREATE PROCEDURE crud_InsertEmployees 
	-- Add the parameters for the stored procedure here
	@UserName VARCHAR(20),
	@FirstName VARCHAR(255),
	@LastName VARCHAR(255),
	@NationalIdNumber VARCHAR(100), 
	@NationalIdType INT, 
	@GenderId INT,
	@BirthDate DATE,
	@Address VARCHAR(100),
	@PlaceId INT,
	@CountryId INT, 
	@UserCreated VARCHAR(20), 
	@DateCreated DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Employees] ([Username], [Firstname], [Lastname], [NationalIDNumber], [NationalIDType], [GenderID], [Birthdate], [Address], [PlaceID], [CountryID], [UserCreated], [DateCreated])
	VALUES (@UserName, @FirstName, @LastName, @NationalIdNumber, @NationalIdType, @GenderId, @BirthDate, @Address, @PlaceId, @CountryId, @UserCreated, @DateCreated)
END