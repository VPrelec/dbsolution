﻿-- =============================================
-- Author:		Vinko Prelec
-- Create date: 26.3.2019.
-- Description:	Procedure for updating data in dbo.NationalIDTypes
-- =============================================
CREATE PROCEDURE crud_UpdateNationalIDTypes
	-- Add the parameters for the stored procedure here
	@NationalIDTypeID INT,
	@NationalIDTypeTitle VARCHAR(100),
	@UserModified VARCHAR(20), 
	@DateModified DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[NationalIDTypes] 
	SET [NationalIDTypeTitle] = @NationalIDTypeTitle
		, [UserModified] = @UserModified
		, [DateModified] = @DateModified 
	WHERE [NationalIDTypeID] = @NationalIDTypeID
END